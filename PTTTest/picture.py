from PIL import Image
import numpy
#import requests #great lib for connecting with a REST interface

def image_test():
    list=["IMG_4476.jpg","IMG_4480.jpg","IMG_4550.jpg"]
    for item in list:
        test_mean(item)


def test_mean(img):
    im = Image.open(img)
    width, height = im.size
    box_list = []       #later contains tuples for easier using of crop-function; used for getting 100x100 areas of the image
    mean_list = []      #later comtains the tuples of the mean color of the 100x100 areas
    counter_list = []     # later comtains the tuples of the mean color of the 100x100 areas
    testimg= Image.open(img)

    #generates tuples for the box_list;
    for x in range(int(width/100)):
        for y in range(int(height/100)):
            box=(x*100,y*100,(x+1)*100,(y+1)*100)
            box_list.append(box)

    pixels=testimg.load()
    for item in box_list:
        imgmean= box_mean(im.crop(item))
        for i in range(item[0],item[2]):
            for j in range(item[1],item[3]):
                pixels[i,j]=imgmean
        mean_list.append(imgmean)       #generates the mean_list
    testimg.show()


    counter_list=rgb_count(mean_list)
    max=rgb_max3(counter_list)


    mean=box_mean(im)
    print(im.getdata())
    print(max)
    max_list=[max[0][0],max[1][0],max[2][0]]
    show_color(max_list)

#Helpfunction; returns the mean rgb valur of a given image (area)
def box_mean(img_region):
    r, g, b = img_region.split() #split in r,g, and b channel

    #mean computing for each channel
    meanvalr = int(numpy.mean(r))
    meanvalg = int(numpy.mean(g))
    meanvalb = int(numpy.mean(b))

    #return as tuple; so values merched again
    return ((meanvalr,meanvalg,meanvalb))


#counts how often a nearly rgb value is part of the picture
def rgb_count(rgbList):
    count_list=[]
    first=rgbList.pop()
    count_list.append([first,1])

    for item in rgbList:
        found = False
        for element in count_list:
            if alike(item,element[0],25):
                element[1] += 1
                found=True
                break
        if not found:
            count_list.append([item,1])
            print(item)
    return count_list

# helpfunktion for better code reading; checks, if the rgb color t1 nearly t2. var is for the accepted difference; returns bool
def alike(t1,t2,var):
    bool0 = (t1[0] >= ((t2[0]) - var)) and (t1[0] <= ((t2[0]) + var))
    bool1 = (t1[1] >= ((t2[1]) - var)) and (t1[1] <= ((t2[1]) + var))
    bool2 = (t1[2] >= ((t2[2]) - var)) and (t1[2] <= ((t2[2]) + var))
    return(bool0 & bool1 & bool2)


def rgb_max3(list):
    max_list=[]
    for i in range(3):
        max = list[0]
        for item in list:
            if item[1]>max[1]:
                max=item
        max_list.append(max)
        list.remove(max)
    return max_list

def show_color(max_list):
    print("maxlist=",max_list)
    im=Image.open("test.jpg")
    pic=im.crop((0,0,600,200))
    pixels=pic.load()
    for x in range(600):
        for y in range(200):
            if x<200:
                pixels[x,y]=max_list[0]
            if x>=200 and x<400:
                pixels[x, y] = max_list[1]
            if(x >= 400):
                pixels[x,y]=max_list[2]
    pic.show()